<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitiowebsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitiowebs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->text('acerca');
            $table->text('logo');
            $table->string('linkedin');
            $table->string('gmail');
            $table->string('twiter');
            $table->string('facebook');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitiowebs');
    }
}
