<?php

namespace App\Http\Controllers;

use App\Models\sitioweb;
use App\Models\Tag;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     /*
    public function __construct()
    {
        $this->middleware('auth');
    }

    */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = view('home')->render(); 
        return $view;
    }


    public function menu(Request $request){

        $item = $request->item;

        switch ($item) {
            case 'auht.login':
                return view($item);
                break;
            case 'auht.register':
                return view($item);
                break;
            case 'admin.sitio.index':
                $sitioweb = sitioweb::first();

                return view($item, compact('sitioweb'));
                break;
            case 'admin.posts.create':

                $categories = Category::pluck('name', 'id')->all();
                $tags = Tag::pluck('name', 'name')->all();
                $populares = $this->populares();

                //return view('admin.posts.create', compact('categories', 'tags', 'populares'));
                return view($item, compact('categories', 'tags', 'populares'));
                break;
            case 'admin.posts.edit':

                $id = $request->id;

                $post = Post::find($id);

                $post = $post->load(['user', 'category', 'tags', 'comments']);

                ///dd($post);
        
                $categories = Category::pluck('name', 'id')->all();
                $tags = Tag::pluck('name', 'name')->all();
        
                return view('admin.posts.edit', compact('post', 'categories', 'tags'));
                //return view($item, compact('categories', 'tags', 'populares'));
                break;
            case 'frontend.index':

                $posts = Post::when($request->search, function($query) use($request) {
                    $search = $request->search;
                    
                    return $query->where('title', 'like', "%$search%")
                        ->orWhere('body', 'like', "%$search%");
                })->with('tags', 'category', 'user')
                ->withCount('comments')
                ->published()
                ->paginate(5);

                $etiquetas = $this->etiquetas();

                $populares = $this->populares();

                $tendencia = $this->tendencia();


                return view($item, compact('posts', 'etiquetas', 'populares', 'tendencia'));

                break;

            case 'frontend.publicaciones':

                $posts = Post::when($request->search, function($query) use($request) {
                    $search = $request->search;
                    
                    return $query->where('title', 'like', "%$search%")
                        ->orWhere('body', 'like', "%$search%");
                })->with('tags', 'category', 'user')
                ->withCount('comments')
                ->published()
                ->order()
                ->paginate(5);

                $etiquetas = $this->etiquetas();

                $populares = $this->populares();

                $tendencia = $this->Tendencia();

                //dd($tendencia);

                return view($item, compact('posts', 'etiquetas', 'populares', 'tendencia'));
                break;
            case 'frontend.mispublicaciones':

                $dato = auth()->user()->id;

                //dd($dato);


                $posts = Post::where('user_id', '=' ,$dato)
                ->with('tags', 'category', 'user')
                ->withCount('comments')
                ->order()
                ->paginate(5);
    
                $etiquetas = $this->etiquetas();
    
                $populares = $this->populares();
    
                $tendencia = $this->Tendencia();
    
                    //dd($posts);
    
                return view($item, compact('posts', 'etiquetas', 'populares', 'tendencia'));
                break;

            case 'frontend.post':

                $post = $post->load(['comments.user', 'tags', 'user', 'category']);

                $etiquetas = $this->etiquetas();

                $populares = $this->populares();

                $tendencia = $this->Tendencia();

                return view($item, compact('post', 'etiquetas', 'populares', 'tendencia'));
                break;
/*
            case 'admin.post.create':
                $usuarios = User::all();
                $roles = Role::all();

                return view($item,compact('usuarios','roles'));
            break;
*/
            default:
                return view($item);
            break;
        }

    }

    public function etiquetas(){

        $tag = Tag::all();

        return $tag;
    }

    public function populares(){

        $posts = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->take(5)
                        ->get();

        ///dd($posts);

        return $posts;
    }

    public function Tendencia(){

        $postst = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->take(5)
                        ->get();

        return $postst;
    }


}
