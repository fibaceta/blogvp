<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['user', 'category', 'tags', 'comments'])->paginate(10);

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id')->all();
        $tags = Tag::pluck('name', 'name')->all();
        $populares = $this->populares();

        return view('admin.posts.create', compact('categories', 'tags', 'populares'));
    }

    public function populares(){

        $posts = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->take(5)
                        ->get();

        return $posts;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {

        //dd($request);
        $url="pordefecto";

        if($request->hasFile('imagen')) {
            //get filename with extension
            $filenamewithextension = $request->file('imagen')->getClientOriginalName();
      
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
      
            //get file extension
            $extension = $request->file('imagen')->getClientOriginalExtension();
      
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            //$filenametostore="logo_nsc".$extension;
      
            //Upload File
            $request->file('imagen')->storeAs('public/uploads', $filenametostore);
 
            $url = asset('storage/uploads/'.$filenametostore);             
        }


        $post = Post::create([
            'title'       => $request->title,
            'body'        => $request->body,
            'category_id' => $request->category_id,
            'imagen' => $url
        ]);

        

        $tagsId = collect($request->tags)->map(function($tag) {
            return Tag::firstOrCreate(['name' => $tag])->id;
        });

        $post->tags()->attach($tagsId);
        flash()->overlay('Post created successfully.');

        //return response()->json($post);

        //return redirect('/admin/posts');
    }

    public function SavePublicacion(Request $request)
    {

        //dd($request);
        $url="pordefecto";

        if($request->hasFile('imagen')) {
            //get filename with extension
            $filenamewithextension = $request->file('imagen')->getClientOriginalName();
      
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
      
            //get file extension
            $extension = $request->file('imagen')->getClientOriginalExtension();
      
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            //$filenametostore="logo_nsc".$extension;
      
            //Upload File
            $request->file('imagen')->storeAs('public/uploads', $filenametostore);
 
            $url = asset('storage/uploads/'.$filenametostore);             
        }


        $post = Post::create([
            'title'       => $request->title,
            'body'        => $request->body,
            'category_id' => $request->category_id,
            'imagen' => $url
        ]);

        

        $tagsId = collect($request->tags)->map(function($tag) {
            return Tag::firstOrCreate(['name' => $tag])->id;
        });

        $post->tags()->attach($tagsId);
        flash()->overlay('Post created successfully.');

        return response()->json($post);

        //return redirect('/admin/posts');
    }

    public function updatePublicacion(Request $request)
    {

        //dd($request);
        $id = $request->id;
        $post = Post::find($id);
        // disbust cache
        Cache::forget($post->etag);


        if($request->hasFile('imagen')) {
            //get filename with extension
            $filenamewithextension = $request->file('imagen')->getClientOriginalName();
      
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
      
            //get file extension
            $extension = $request->file('imagen')->getClientOriginalExtension();
      
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            //$filenametostore="logo_nsc".$extension;
      
            //Upload File
            $request->file('imagen')->storeAs('public/uploads', $filenametostore);
 
            $url = asset('storage/uploads/'.$filenametostore);        
            
            $post->update(['imagen'=>$url]);
        }

        $post->update([
            'title'       => $request->title,
            'body'        => $request->body,
            'category_id' => $request->category_id,
        ]);

        $tagsId = collect($request->tags)->map(function($tag) {
            return Tag::firstOrCreate(['name' => $tag])->id;
        });

        $post->tags()->sync($tagsId);
        flash()->overlay('Publicacion Actualizada!.');

        //return redirect('/admin/posts');
    }

    public function siguiente(Request $request){

        //dd($request);
        if($request->ajax()){

            $dato = auth()->user()->id;

            $posts = Post::where('user_id', '=' ,$dato)
                ->with('tags', 'category', 'user')
                ->withCount('comments')
                ->order()
                ->paginate(5);
    
                $etiquetas = $this->etiquetas();
    
                $populares = $this->populares();
    
                $tendencia = $this->Tendencia();
    
                    //dd($posts);
    
                return view('frontend._mipost', compact('posts', 'etiquetas', 'populares', 'tendencia'))->render();
            //$data = DB::table('sample_datas')->simplePaginate(5);
            //return view('pagination_child', compact('data'))->render();
        }
    }

    public function etiquetas(){

        $tag = Tag::all();

        return $tag;
    }


    public function Tendencia(){

        $postst = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->take(5)
                        ->get();

        return $postst;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post = $post->load(['user', 'category', 'tags', 'comments']);

        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        if($post->user_id != auth()->user()->id && auth()->user()->is_admin == false) {
            flash()->overlay("You can't edit other peoples post.");
            return redirect('/admin/posts');
        }

        $categories = Category::pluck('name', 'id')->all();
        $tags = Tag::pluck('name', 'name')->all();

        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        // disbust cache
        Cache::forget($post->etag);

        $post->update([
            'title'       => $request->title,
            'body'        => $request->body,
            'category_id' => $request->category_id
        ]);

        $tagsId = collect($request->tags)->map(function($tag) {
            return Tag::firstOrCreate(['name' => $tag])->id;
        });

        $post->tags()->sync($tagsId);
        flash()->overlay('Publicacion Actualizada!.');

        //return redirect('/admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if($post->user_id != auth()->user()->id && auth()->user()->is_admin == false) {
            flash()->overlay("You can't delete other peoples post.");
            return redirect('/admin/posts');
        }

        $post->delete();
        flash()->overlay('Post deleted successfully.');

        return redirect('/admin/posts');
    }

    public function publish(Post $post)
    {
        $post->is_published = !$post->is_published;
        $post->save();
        flash()->overlay('Post changed successfully.');

        return redirect('/admin/posts');
    }
}
