<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use App\Models\Category;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $posts = Post::when($request->search, function($query) use($request) {
                        $search = $request->search;
                        
                        return $query->where('title', 'like', "%$search%")
                            ->orWhere('body', 'like', "%$search%");
                    })->with('tags', 'category', 'user')
                    ->withCount('comments')
                    ->published()
                    ->order()
                    ->paginate(5);

        $etiquetas = $this->etiquetas();

        $populares = $this->populares();

        $tendencia = $this->tendencia();


        return view('frontend.index', compact('posts', 'etiquetas', 'populares', 'tendencia'));
    }

    public function post(Post $post)
    {
        $post = $post->load(['comments.user', 'tags', 'user', 'category']);

        $etiquetas = $this->etiquetas();

        $populares = $this->populares();

        $tendencia = $this->tendencia();

        //dd($post);

        return view('frontend.post', compact('post', 'etiquetas', 'populares', 'tendencia'));
    }

    public function comment(Request $request, Post $post)
    {
        $this->validate($request, ['body' => 'required']);

        $post->comments()->create([
            'body' => $request->body
        ]);
        //flash()->overlay('Commentario Creado');

        dd('ok');

        return redirect("/posts/{$post->id}");
    }

    public function comentar(Request $request)
    {

        $id = $request->id;
        $post = Post::find($id);
        

        $this->validate($request, ['body' => 'required']);

        $post->comments()->create([
            'body' => $request->body
        ]);
        //flash()->overlay('Commentario Creado');
        //flash('Commentario Creado');

        $post = $post->load(['comments.user', 'tags', 'user', 'category']);

        //return response()->json($post);

        return view('frontend._comments', compact('post'))->render();

        //dd('ok');

        //return redirect("/posts/{$post->id}");
    }


    public function publicaciones(Request $request)
    {
        $posts = Post::when($request->search, function($query) use($request) {
                        $search = $request->search;
                        
                        return $query->where('title', 'like', "%$search%")
                            ->orWhere('body', 'like', "%$search%");
                    })->with('tags', 'category', 'user')
                    ->withCount('comments')
                    ->published()
                    ->order()
                    ->paginate(5);

        $etiquetas = $this->etiquetas();

        $populares = $this->populares();

        $tendencia = $this->tendencia();

        //dd($tendencia);

        return view('frontend.publicaciones', compact('posts', 'etiquetas', 'populares', 'tendencia'));
    }

    public function siguiente(Request $request){

        //dd($request);
        if($request->ajax()){

            $posts = Post::when($request->search, function($query) use($request) {
                $search = $request->search;
                
                return $query->where('title', 'like', "%$search%")
                    ->orWhere('body', 'like', "%$search%");
            })->with('tags', 'category', 'user')
            ->withCount('comments')
            ->published()
            ->order()
            ->paginate(5);
    
                $etiquetas = $this->etiquetas();
    
                $populares = $this->populares();
    
                $tendencia = $this->Tendencia();
    
                    //dd($posts);
    
                return view('frontend._post', compact('posts', 'etiquetas', 'populares', 'tendencia'))->render();
            //$data = DB::table('sample_datas')->simplePaginate(5);
            //return view('pagination_child', compact('data'))->render();
        }
    }


    public function etiquetas(){

        $tag = Tag::all();

        return $tag;
    }

    public function populares_ini(){

        $populares = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->take(5)
                        ->get();

        return view('frontend._populares', compact('populares'));
    }

    public function populares(){

        $posts = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->take(11)
                        ->get();

        return $posts;
    }

    public function popular(){

        $populares = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->first();

        //dd($populares);



        if($populares->id){

            /*$menu="<div class='col-12 fh5co_mediya_center'>".
            "<a href='#' class='color_fff fh5co_mediya_setting'><i class='fa fa-clock-o'></i>&nbsp;&nbsp;&nbsp;".now()->toDayDateTimeString()."</a>".                
            "<div class='d-inline-block fh5co_trading_posotion_relative'><a href='#' class='treding_btn'><i class='fa fa-search'></i> </a></div>".
            "<a href='javascript:void(0)' onclick='App.Post({{$populares->id}})'  class='color_fff fh5co_mediya_setting'>$populares->title</a>".                
            "</div>";*/
            return view('layouts._header', compact('populares'));


        }else{
            $menu="<div class='col-9 fh5co_mediya_center'>".
            "<a href='#' class='color_fff fh5co_mediya_setting'><i class='fa fa-clock-o'></i>&nbsp;&nbsp;&nbsp;".now()->toDayDateTimeString()."</a>".                
            "<a href='".url('/admin/posts/create')."' class='color_fff fh5co_mediya_setting'>Nueva Publicacion</a>".
            "</div>".
            "<div class='col-3 fh5co_mediya_right'>".
            
            "</div>";

        }

       
        echo $menu;
        //return $populares;

    }

    public function categorias()
    {
        $categories = Category::withCount('posts')->get();

        $menu="<ul class='footer_menu'>";
        foreach ($categories as $cat) {
            $menu=$menu."<li><a href='#' class=''><i class='fa fa-angle-right'></i>&nbsp;&nbsp; ".$cat->name."</a></li>";
        }        
        $menu=$menu."</ul>";

        echo $menu;
    }

    public function masVistos()
    {
        $populares = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->take(3)
                        ->get();

        $menu="";
        foreach ($populares as $cat) {
            $menu=$menu."<div class='footer_makes_sub_font'>".$cat->created_at->toDayDateTimeString()."</div>";
            $menu=$menu."<a onclick='App.Post({$cat->id})' href='javascript:void(0)' class='footer_post pb-4'> $cat->title </a>";
        }
        echo $menu;
    }

    public function modificados()
    {
        $populares = Post::orderby('updated_at', 'desc')
                        ->published()
                        ->take(9)
                        ->get();

        $menu="";
        foreach ($populares as $cat) {
            $menu=$menu."<a onclick='App.Post({$cat->id})' href='javascript:void(0)' class='footer_img_post_6'><img src='".asset($cat->imagen)."' alt='img'/></a>";
        }
        echo $menu;
    }

    public function Tendencia(){

        $postst = Post::withCount('comments')
                        ->orderby('comments_count', 'desc')
                        ->published()
                        ->take(5)
                        ->get();

        return $postst;
    }

}
