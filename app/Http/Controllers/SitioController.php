<?php

namespace App\Http\Controllers;

use App\Models\sitioweb;
use Illuminate\Http\Request;

class SitioController extends Controller
{
    public function index()
    {
        $sitioweb = sitioweb::first();

        return view('admin.sitio.index', compact('sitioweb'));
    }


    public function upload(Request $request)
    {
        //dd($request);
        if($request->id){

            $sitioweb = sitioweb::find($request->id);

        }else{
            $sitioweb = new sitioweb;
        }
        
        

        if($request->hasFile('logo')) {
            //get filename with extension

            //dd($request->logo);
            $filenamewithextension = $request->file('logo')->getClientOriginalName();
      
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
      
            //get file extension
            $extension = $request->file('logo')->getClientOriginalExtension();
      
            //filename to store
            //$filenametostore = $filename.'_'.time().'.'.$extension;
            $filenametostore="logo_nsc".$extension;
      
            //Upload File
            $request->file('logo')->storeAs('public/uploads', $filenametostore);
 
            $url = asset('storage/uploads/'.$filenametostore); 

            $sitioweb->logo = $url;

            $msg = 'Imagen cargada'; 

            
        }

        //dd($request);

        $sitioweb->acerca = $request->acerca;
        $sitioweb->linkedin = $request->linkedin;
        $sitioweb->gmail = $request->gmail;
        $sitioweb->twiter = $request->twiter;
        $sitioweb->facebook = $request->facebook;
        $sitioweb->save();

        return response()->json($sitioweb);

        //return view('admin.sitio.index', compact('sitioweb'));
    }

    

    public function sitioweb()
    {
        $sitioweb = sitioweb::first();

        return response()->json($sitioweb);
    }


}
