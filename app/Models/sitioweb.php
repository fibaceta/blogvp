<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class sitioweb extends Model
{
    protected $fillable = [
        'titulo',
        'acerca',
        'logo',
        'linkedin',
        'gmail',
        'twiter',
        'facebook'
    ];
}
