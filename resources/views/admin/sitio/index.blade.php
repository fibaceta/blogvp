<br>
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>
                            Configuración del Sitio Web
                        </h2>
                    </div>

                    <div class="panel-body">
                    {!! Form::open(['id' => 'sitio_web_form', 'role' => 'form', 'files'=> 'true', 'enctype'=>'multipart/form-data']) !!}
                    {{ csrf_field() }}
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Atributo</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                            {{ Form::hidden('id', $sitioweb->id) }}
                                                                
                                <tr>
                                    <td>{!! Form::label('acerca', 'acerca', ['class' => 'col-md-1 control-label']) !!}</td>
                                    <td>{!! Form::textarea('acerca', $sitioweb->acerca, ['class' => 'form-control', 'required']) !!}</td>
                                </tr>
                                <tr>
                                    <td>{!! Form::label('logo', 'logo', ['class' => 'col-md-1 control-label']) !!}</td>
                                    <td><img alt="img" id="previa" class="fh5co_logo_width" src="{{ asset('storage/uploads/logo_nsc.png') }}">{!! Form::file('logo', null, ['class' => 'form-control', 'required']) !!}</td>
                                </tr>
                                <tr>
                                    <td>{!! Form::label('linkedin', 'linkedin', ['class' => 'col-md-1 control-label']) !!}</td>
                                    <td>{!! Form::text('linkedin', $sitioweb->linkedin, ['class' => 'form-control', 'required']) !!}</td>
                                </tr>

                                <tr>
                                    <td>{!! Form::label('gmail', 'gmail', ['class' => 'col-md-1 control-label']) !!}</td>
                                    <td>{!! Form::text('gmail', $sitioweb->gmail, ['class' => 'form-control', 'required']) !!}</td>
                                </tr>

                                <tr>
                                    <td>{!! Form::label('twiter', 'twiter', ['class' => 'col-md-1 control-label']) !!}</td>
                                    <td>{!! Form::text('twiter', $sitioweb->twiter, ['class' => 'form-control', 'required']) !!}</td>
                                </tr>
                                <tr>
                                    <td>{!! Form::label('facebook', 'facebook', ['class' => 'col-md-1 control-label']) !!}</td>
                                    <td>{!! Form::text('facebook', $sitioweb->facebook, ['class' => 'form-control', 'required']) !!}</td>
                                </tr>
                                <tr>
                                    <td><input type="button" onclick="App.SaveSitioWeb()" class="btn btn-primary" value="Subir"></td>                                
                                </tr>
                               
                            </tbody>
                        </table>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
