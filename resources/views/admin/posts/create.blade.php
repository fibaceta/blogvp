
<br>
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>
                            Crear Nueva Publicación

                            
                        </h2>
                    </div>

                    <div class="panel-body">
                        {!! Form::open(['id'=>'publicacion_form', 'class' => 'form-horizontal', 'role' => 'form', 'files'=> 'true', 'enctype'=>'multipart/form-data']) !!}

                            @include('admin.posts._form')

                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-0">
                                    <!--<button type="submit" class="btn btn-primary">Guardar</button>--->
                                    <button onclick="Publicacion.formSave()" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
	
    $(document).ready(function() {
    
        Publicacion = {
            url_publicacion:"{{ url('/Crear_publicacion') }}",
            //menu:$('a.menu_item.active'),
            token:"{{csrf_token()}}",
            init:function(){
                $('form').on('submit', function (ev) {
                  ev.preventDefault();
                });
                App.Editor();
            },
            formSave:function(){

                var publicacion = CKEDITOR.instances.body.getData();
                
                $("#body").val(publicacion);

                var formElement = document.getElementById("publicacion_form");
                
                var ajax = App.ajaxData(this.url_publicacion,formElement);
    
                if (ajax){
                    ajax.done(function(data){
                        App.ir_a('frontend.mispublicaciones');
                        
                    });
                }
            }
        }
        
        Publicacion.init();

    });
    
    </script>