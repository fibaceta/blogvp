<div class="container-fluid bg-faded fh5co_padd_mediya padding_786">
    <div class="container padding_786">
        <nav class="navbar navbar-toggleable-md navbar-light ">
            <button class="navbar-toggler navbar-toggler-right mt-3" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation"><span class="fa fa-bars"></span></button>
            <a class="navbar-brand" href="{{ route('inicio') }}"><img src="{{ asset('/blog/images/logo_nsc.png') }}" alt="img" class="mobile_logo_width"/></a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto" id="sidebarnav">
                    @if (Auth::user())
                        <li class="nav-item ">
                            <!---<a class="nav-link"  href="{{ url('admin/posts/create') }}">Publicar <span class="sr-only">(current)</span></a>--->
                            <a class="nav-link" onclick="App.publicar();" href="javascript:void(0)">Publicar <span class="sr-only">(current)</span></a>
                        </li>
                        
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('inicio') }}">Voz Pública <span class="sr-only">(current)</span></a>
                        <!--<a class="nav-link" data-item="frontend.index" href="javascript:void(0)">Home <span class="sr-only">(current)</span></a>--->
                    </li>
                    <li class="nav-item">
                        <!---<a class="nav-link" href="{{ route('publicaciones') }}">Publicaciones <span class="sr-only">(current)</span></a>-->
                        <a class="nav-link  nav-click" data-item="frontend.publicaciones" href="javascript:void(0)">Publicaciones <span class="sr-only">(current)</span></a>
                    </li>


                    <!--
                    <li class="nav-item ">
                        <a class="nav-link" href="blog.html">Blog <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="single.html">Single <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdownMenuButton2" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">World <span class="sr-only">(current)</span></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink_1">
                            <a class="dropdown-item" href="#">Action in</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdownMenuButton3" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">Community<span class="sr-only">(current)</span></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink_1">
                            <a class="dropdown-item" href="#">Action in</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="Contact_us.html">Contact <span class="sr-only">(current)</span></a>
                    </li>
                    ------>
                    @if (Auth::user())
                        @if (Auth::user()->is_admin)
                            

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="dropdownMenuButton3" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Configurar<span class="sr-only">(current)</span></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink_1">
                                    <a class="dropdown-item nav-click" data-item="admin.sitio.index" href="javascript:void(0)" >sitio Web</a>
                                    <a class="dropdown-item" href="{{ url('admin/posts') }}">Posts</a>
                                    <a class="dropdown-item" href="{{ url('admin/categories') }}">Categories</a>
                                    <a class="dropdown-item" href="{{ url('admin/comments') }}">Comments</a>
                                    <a class="dropdown-item" href="{{ url('admin/tags') }}">Tags</a>
                                    <a class="dropdown-item" href="{{ url('admin/users') }}">Users</a>
                                
                                </div>
                            </li>
                        @endif
                    @endif
                    
                    @if (Auth::guest())


                    <li class="nav-item dropdown">

                        <a class="nav-link dropdown-toggle nav-click" href="javascript:void(0)" id="dropdownMenuButton3" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false" data-item="auth.login">Ingreso / Registro<span class="sr-only">(current)</span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink_1">
                            <a class="dropdown-item nav-click" data-item="auth.login" href="javascript:void(0)" >Ingreso</a>
                            <a class="dropdown-item nav-click" data-item="auth.register" href="javascript:void(0)" >Registro</a>
                        </div>
                    
                    </li>
                    

                    @else
                        <li class="nav-item dropdown">

                        <a class="nav-link dropdown-toggle" href="#" id="dropdownMenuButton3" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}<span class="sr-only">(current)</span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink_1">
                            <a class="dropdown-item nav-click" data-item="frontend.mispublicaciones" href="javascript:void(0)" >Mis Publicaciones</a>
                            <a class="dropdown-item" href="{{ url('/profile') }}">Perfil</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                        Salir
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            
                        </div>
                    
                        </li>
                    @endif
                    

                </ul>
            </div>
        </nav>
    </div>
</div>