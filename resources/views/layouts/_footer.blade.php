<div class="container-fluid fh5co_footer_bg pb-3">
    <div class="container animate-box">

        <div class="row">
            <div class="col-12 spdp_right py-5"><img src="{{ asset('/blog/images/logo_blanco.png') }}" id="logo_web2" alt="img" class="footer_logo"/></div>
            <div class="clearfix"></div>
            <div class="col-12 col-md-4 col-lg-3">
                <div class="footer_main_title py-3"> Acerca de..</div>
                <div class="footer_sub_about pb-3" id='acerca_sitio'>
                </div>
                <div class="footer_mediya_icon">
                    <div class="text-center d-inline-block"><a class="fh5co_display_table_footer">
                        <div class="fh5co_verticle_middle"><i class="fa fa-linkedin"></i></div>
                    </a></div>
                    <div class="text-center d-inline-block"><a class="fh5co_display_table_footer">
                        <div class="fh5co_verticle_middle"><i class="fa fa-google-plus"></i></div>
                    </a></div>
                    <div class="text-center d-inline-block"><a class="fh5co_display_table_footer">
                        <div class="fh5co_verticle_middle"><i class="fa fa-twitter"></i></div>
                    </a></div>
                    <div class="text-center d-inline-block"><a class="fh5co_display_table_footer">
                        <div class="fh5co_verticle_middle"><i class="fa fa-facebook"></i></div>
                    </a></div>
                </div>
            </div>
            <div class="col-12 col-md-3 col-lg-2">
                <div class="footer_main_title py-3"> Categorias</div>
                <div id="categorias"></div>
            </div>
            <div class="col-12 col-md-5 col-lg-3 position_footer_relative">
                <div class="footer_main_title py-3"> Publicaciones mas Vistas</div>
                <div id="masVistos"></div>                              
                <div class="footer_position_absolute"><img src="{{ asset('blog/images/footer_sub_tipik.png') }}" alt="img" class="width_footer_sub_img"/></div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 ">
                <div class="footer_main_title py-3"> Publicaciones modificadas Ultimamente</div>
                <div id="modificados"></div>
            </div>
        </div>
<!--
        <div class="row justify-content-center pt-2 pb-4">
            <div class="col-12 col-md-8 col-lg-7 ">
                <div class="input-group">
                    <span class="input-group-addon fh5co_footer_text_box" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control fh5co_footer_text_box" placeholder="Escribe tu email..." aria-describedby="basic-addon1">
                    <a href="#" class="input-group-addon fh5co_footer_subcribe" id="basic-addon12"> <i class="fa fa-paper-plane-o"></i>&nbsp;&nbsp;Subscribirme</a>
                </div>
            </div>
        </div>

        --->

    </div>
</div>


<div class="container-fluid fh5co_footer_right_reserved">
    <div class="container">
        <div class="row" id="sidebarnav">
            <div class="col-12 col-md-6 py-4 Reserved"> © Copyright 2020, All rights reserved. Design by <a href="#" title="Francisco Ibaceta">Francisco Ibaceta</a>. </div>
            <div class="col-12 col-md-6 spdp_right py-4">
                <a href="{{ route('inicio') }}" class="footer_last_part_menu">Home</a>
                <a data-item="frontend.publicaciones" href="javascript:void(0)" class="footer_last_part_menu nav-click">publicaciones</a>
                <a href="Contact_us.html" class="footer_last_part_menu">Contact</a>
                <a href="blog.html" class="footer_last_part_menu">Latest News</a></div>
        </div>
    </div>
</div>