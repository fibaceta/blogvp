<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Voz Publica') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('blog/css/media_query.css') }}" rel="stylesheet"  type="text/css"/>
    <link href="{{ asset('blog/css/bootstrap.css') }}" rel="stylesheet"  type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link href="{{ asset('blog/css/animate.css') }}" rel="stylesheet"  type="text/css"/> 
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

    <link href="{{ asset('blog/css/owl.carousel.css') }}" rel="stylesheet"  type="text/css"/>
    <link href="{{ asset('blog/css/owl.theme.default.css') }}" rel="stylesheet"  type="text/css"/>
 
    <!-- Bootstrap CSS -->
    <link href="{{ asset('blog/css/style_1.css') }}" rel="stylesheet"  type="text/css"/>
    <link href="{{ asset('blog/css/stylenew.css') }}" rel="stylesheet"  type="text/css"/>
    <!-- Modernizr JS -->
    <script src="{{ asset('blog/js/modernizr-3.5.0.min.js') }}"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body>


<!----CABECERA------->
<div class="container-fluid fh5co_header_bg">
    <div class="container">
        <div class="row">
            <div class="col-12 fh5co_mediya_center">
                <a href="#" class="color_fff fh5co_mediya_setting header_btn">
                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;&nbsp;{{ now()->toDayDateTimeString() }}
                </a>
                <!--
                <div class="d-inline-block fh5co_trading_posotion_relative"><a href="#" class="treding_btn">Mas Visto </a>
                    <div class="fh5co_treding_position_absolute"></div>
                </div>       --->      
                
            </div>
        </div>
    </div>
</div>

<!----- REDES SOCIALES---->
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 fh5co_padding_menu">
            <a href="{{ route('inicio') }}">
                <img src="{{ asset('/blog/images/logo_nsc.png') }}" id="logo_web" alt="img" class="fh5co_logo_width"/>
            </a>
            </div>
            <div class="col-12 col-md-9 align-self-center fh5co_mediya_right">
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table"><div class="fh5co_verticle_middle"><i class="fa fa-search"></i></div></a>
                </div>
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table"><div class="fh5co_verticle_middle"><i class="fa fa-linkedin"></i></div></a>
                </div>
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table"><div class="fh5co_verticle_middle"><i class="fa fa-google-plus"></i></div></a>
                </div>
                <div class="text-center d-inline-block">
                    <a href="https://twitter.com/fh5co" target="_blank" class="fh5co_display_table"><div class="fh5co_verticle_middle"><i class="fa fa-twitter"></i></div></a>
                </div>
                <div class="text-center d-inline-block">
                    <a href="https://fb.com/fh5co" target="_blank" class="fh5co_display_table"><div class="fh5co_verticle_middle"><i class="fa fa-facebook"></i></div></a>
                </div>
                
                <div class="d-inline-block text-center dd_position_relative ">
                    
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>


<!----- MENU ------>
@include('layouts._menu')

<!-------- MENSAJE ------>
<div class="container">
    <div class="row">
        @include('flash::message')
    </div>
</div>

<div id="app_container">
    @yield('content')

</div>

<!--------FOOOTER ---->
@include('layouts._footer')



<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="fa fa-arrow-up"></i></a>
</div>



<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="{{ asset('blog/js/owl.carousel.min.js') }}"></script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!-- Waypoints -->
<script src="{{ asset('blog/js/jquery.waypoints.min.js') }}"></script>

<!-- Parallax -->
<script src="{{ asset('blog/js/jquery.stellar.min.js') }}"></script>
<!-- Main -->
<script src="{{ asset('blog/js/main.js') }}"></script>



<script>

var App = {
        menu_url:"{{ route('menu_url') }}",
        token:"{{csrf_token()}}",
        init:function(){
            
            app = this;

            $('#sidebarnav a.nav-click').on('click',function(){

                $('#sidebarnav a').removeClass('active');

                var item = $(this).data('item');

                $(this).addClass('active');

                //App.Actualiza_popular();
                //App.Actualiza_acercade();             

                $('#app_container').html('');
                //$('#app_container').html($('.preloader').clone().show());

                $.ajax({
                    url:app.menu_url,
                    type:'POST',
                    data:{
                        item:item,
                        _token:"{{ csrf_token() }}"
                    }
                }).done(function(data){
                    //$('.preloader').hide();
                    $('#app_container').html(data);
                })

            });

            //this.Actualiza_popular();
            this.Actualiza_acercade();  
            this.publicaciones(); 

        },
        publicar:function(){
            $('#sidebarnav a').removeClass('active');
            $('#app_container').html('');
                //$('#app_container').html($('.preloader').clone().show());
            var item = "admin.posts.create";
            $.ajax({
                url:app.menu_url,
                type:'POST',
                data:{
                    item:item,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                    //$('.preloader').hide();
                $('#app_container').html(data);
                
            });

        },
        ir_a:function(item){
            $('#app_container').html('');
            $.ajax({
                url:app.menu_url,
                type:'POST',
                data:{
                    item:item,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                    //$('.preloader').hide();
                $('#app_container').html(data);
                
            });

        },
        publicaciones:function(){

            $('#publicaciones_ini').html('');
            $.ajax({
                url:"{{ route('publicaciones') }}",
                type:'GET',
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                        //$('.preloader').hide();
                $('#publicaciones_ini').html(data);
                    
            });
        },
        Editor:function(id){

            CKEDITOR.replace( 'body', {
                    customConfig: ''
                });
        
        },
        Editar_publicacion:function(id){
            $('#sidebarnav a').removeClass('active');
            $('#app_container').html('');
                //$('#app_container').html($('.preloader').clone().show());
            var item = "admin.posts.edit";
            $.ajax({
                url:app.menu_url,
                type:'POST',
                data:{
                    item:item,
                    id:id,
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                    //$('.preloader').hide();
                $('#app_container').html(data);
                //App.Editor();
            });

        },
        populares_ini:function(){

            $('#populares_inicio').html("");

            $.ajax({
                url:"{{ route('populares.inicio') }}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                $('#populares_inicio').html(data);
                

            });

        },        
        Actualiza_popular:function(){
            $.ajax({
                url:"{{url('popular')}}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                $('#popular').html(data);
                

            });

        },
        Actualiza_acercade:function(){

            $.ajax({
                url:"{{url('sitioweb')}}",
                type:"POST",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                $('#acerca_sitio').html(data.acerca);
            });

            $.ajax({
                url:"{{url('categorias')}}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){

                $('#categorias').html(data);


            });

            $.ajax({
                url:"{{url('masVistos')}}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){

                $('#masVistos').html(data);


            });

            $.ajax({
                url:"{{url('modificados')}}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){

                $('#modificados').html(data);


            });

        },
        Post:function(id){

                $('#app_container').html('');
                //$('#app_container').html($('.preloader').clone().show());

                $.ajax({
                    url:"{{url('posts')}}/"+id,
                    type:'POST',
                    data:{
                        id:id,
                        _token:"{{ csrf_token() }}"
                    }
                }).done(function(data){
                    //$('.preloader').hide();
                    $('#app_container').html(data);
                });
        },
        abrir_menu:function(){
            var left = $('.left-sidebar').css('left');
            if (left == '0px'){
                $('.left-sidebar').css('left','-240px');
            }else{
                $('.left-sidebar').css('left','0px');
            }
        },
        SaveSitioWeb:function(){

            var url = "{{ url('Actualizar_sitio') }}";
            var formElement = document.getElementById("sitio_web_form");

            var ajax = App.ajaxData(url,formElement);

            if (ajax){
                ajax.done(function(data){
                    alert("Datos Actualizados");
                    
                });
            }

        },
        ajaxData:function(url,form){
            console.log(url);
            formData = new FormData(form);
            
            if( form.reportValidity() ) {
                var ajax = $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    processData: false,  
                    contentType: false   
                });
                return ajax;
            }

            return false;
        }
    }

    App.init();
    
/*
    $(document).ready(function() {


            $.ajax({
                url:"{{url('sitioweb')}}",
                type:"POST",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                
                //alert(data.acerca);
                $('#acerca_sitio').html(data.acerca);
                //$("#logo_web").attr("src",data.logo);
                //$("#logo_web2").attr("src",data.logo);
            });

            $.ajax({
                url:"{{url('popular')}}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){
                $('#popular').html(data);

            });

            $.ajax({
                url:"{{url('categorias')}}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){

                $('#categorias').html(data);


            });

            $.ajax({
                url:"{{url('masVistos')}}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){

                $('#masVistos').html(data);


            });

            $.ajax({
                url:"{{url('modificados')}}",
                type:"GET",
                data:{
                    _token:"{{ csrf_token() }}"
                }
            }).done(function(data){

                $('#modificados').html(data);


            });


            



    });*/

    

</script>
</body>
</html>