<div align="right">
                        {!! $posts->links() !!}
                    </div>
@forelse ($posts as $post)
                <div class="row pb-4">
                    <div class="col-md-5">
                        <div class="fh5co_hover_news_img">
                            <div class="fh5co_news_img" onclick="App.Post({{$post->id}})"><img src="{{ asset($post->imagen) }}" alt=""/></div>
                            <div></div>
                        </div>
                    </div>
                    <div class="col-md-7 animate-box">
                        <a onclick="App.Post({{$post->id}})" href="javascript:void(0)" class="fh5co_magna py-2">{{ $post->title }}</a> 
                        <a onclick="App.Post({{$post->id}})" href="javascript:void(0)" class="fh5co_mini_time py-3"> {{ $post->user->name }} - {{ $post->created_at->toDayDateTimeString() }} </a>
                        <div class="fh5co_consectetur"> {!! strip_tags(str_limit($post->body, 100)) !!}</div>
                        <p>
                                Etiquetas:
                                @forelse ($post->tags as $tag)
                                    <span class="label label-default">{{ $tag->name }}</span>
                                @empty
                                    <span class="label label-danger">Sin Etiquetas.</span>
                                @endforelse
                            </p>
                            <p>
                                <span class="btn btn-sm btn-success">{{ $post->category->name }}</span>
                                <span class="btn btn-sm btn-info">Comentarios <span class="badge">{{ $post->comments_count }}</span></span>

                                <a onclick="App.Post({{$post->id}})" href="javascript:void(0)" class="btn btn-sm btn-primary">Ver ..</a>
                            </p>
                            <p>
                                @if($post->is_published)
                                <a href="javascript:void(0)" class="btn btn-success">
                                <svg class="bi bi-info-circle-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                </svg>
                                Publicado</a>
                                @else
                                <a href="javascript:void(0)" class="btn btn-danger">
                                <svg class="bi bi-info-circle-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                </svg>
                                No Publicado</a>
                                <a href="javascript:void(0)" onclick="App.Editar_publicacion({{$post->id}})" class="btn btn-info">
                                <svg class="bi bi-arrow-down-left-square-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm7.5 11h-4a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 1 0v2.793l4.146-4.147a.5.5 0 0 1 .708.708L6.707 10H9.5a.5.5 0 0 1 0 1z"/>
                                </svg>
                                Editar</a>
                                @endif
                            </p>
                    </div>
                </div>
                @empty
                    <div class="panel panel-default">
                        <div class="panel-heading">Not Found!!</div>

                        <div class="panel-body">
                            <p>Sorry! No post found.</p>
                        </div>
                    </div>
                @endforelse

                <div align="right">
                        {!! $posts->links() !!}
                    </div>