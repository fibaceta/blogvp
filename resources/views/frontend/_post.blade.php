<div align="left">
    {!! $posts->links() !!}
</div>
@forelse ($posts as $post)
                <div class="row pb-4">
                    <div class="col-md-5">
                        <div class="fh5co_hover_news_img">
                            <div class="fh5co_news_img" onclick="App.Post({{$post->id}})"><img src="{{ asset($post->imagen) }}" alt=""/></div>
                            <div></div>
                        </div>
                    </div>
                    <div class="col-md-7 animate-box">
                        <a onclick="App.Post({{$post->id}})" href="javascript:void(0)" class="fh5co_magna py-2">{{ $post->title }}</a> 
                        <a onclick="App.Post({{$post->id}})" href="javascript:void(0)" class="fh5co_mini_time py-3"> {{ $post->user->name }} - {{ $post->created_at->toDayDateTimeString() }} </a>
                        <div class="fh5co_consectetur"> {!! strip_tags(str_limit($post->body, 100)) !!}</div>
                        <p>
                                Etiquetas:
                                @forelse ($post->tags as $tag)
                                    <span class="label label-default">{{ $tag->name }}</span>
                                @empty
                                    <span class="label label-danger">Sin Etiquetas.</span>
                                @endforelse
                            </p>
                            <p>
                                <span class="btn btn-sm btn-success">{{ $post->category->name }}</span>
                                <span class="btn btn-sm btn-info">Comentarios <span class="badge">{{ $post->comments_count }}</span></span>

                                <a onclick="App.Post({{$post->id}})" href="javascript:void(0)" class="btn btn-sm btn-primary">Ver..</a>
                            </p>
                    </div>
                </div>
                @empty
                    <div class="panel panel-default">
                        <div class="panel-heading">Not Found!!</div>

                        <div class="panel-body">
                            <p>Sorry! No post found.</p>
                        </div>
                    </div>
                @endforelse
<div align="center">
    {!! $posts->appends(['search' => request()->get('search')])->links() !!}
</div>