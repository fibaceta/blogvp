@forelse ($post->comments as $comment)
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ $comment->user->name }} dice...

            <span class="pull-right">{{ $comment->created_at->diffForHumans() }}</span>
        </div>

        <div class="panel-body">
            <p>{!! $comment->body !!}</p>
        </div>
    </div>
@empty
    <div class="panel panel-default">
        <div class="panel-heading">No Encontrado!!</div>

        <div class="panel-body">
            <p>Lo siento! No se necontraron comentarios para esta publicacion.</p>
        </div>
    </div>
@endforelse
