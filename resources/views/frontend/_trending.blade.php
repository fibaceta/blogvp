<div class="container-fluid pb-4 pt-5">
    <div class="container animate-box">
        <div>
            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Tendencia</div>
        </div>
        <div class="owl-carousel owl-theme" id="slider2">
            @forelse($tendencia as $tend)
            <div class="item px-2">
                <a href="{{ url("/posts/{$tend->id}") }}">
                <div class="fh5co_hover_news_img">
                    <div class="fh5co_news_img"><img src="{{ asset($tend->imagen) }}" alt=""/></div>
                    <div>
                        <a href="#" class="d-block fh5co_small_post_heading"><span class="">{{ $tend->title }}</span></a>
                        <div class="c_g"><i class="fa fa-clock-o"></i>{{ $tend->created_at->toDayDateTimeString() }}</div>
                    </div>
                </div>
                </a>

            </div>
            @empty
                <p> Sin Tendencia!! </p>
            @endforelse

        </div>
    </div>
</div>