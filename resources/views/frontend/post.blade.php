<div id="fh5co-single-content" class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4"><h2>{{ $post->title }} - <small>by {{ $post->user->name }}</small></h2></div>
            <span>{{ $post->created_at->toDayDateTimeString() }}</span>
                <p>{!! $post->body !!}</p>
                <p>
                            Category: <span class="label label-success">{{ $post->category->name }}</span> <br>
                            Tags:
                            @forelse ($post->tags as $tag)
                                <span class="label label-default">{{ $tag->name }}</span>
                            @empty
                                <span class="label label-danger">No tag found.</span>
                            @endforelse
                        </p>

                @includeWhen(Auth::user(), 'frontend._form')
                <br>
                <div id="panelComentarios">
                @include('frontend._comments')
                </div>
            </div>

            @include('frontend._fadeRight')

        </div>
    </div>
</div>

@include('frontend._trending')