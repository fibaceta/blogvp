<br>
<div class="panel panel-default">
    <div class="panel-heading">Escribe tus comentarios</div>

    <div class="panel-body">
        {!! Form::open(['id'=>'comentario_form']) !!}
            <div class="form-group">
            {!! Form::hidden('id', $post->id, ['required']) !!}
                {!! Form::textarea('body', null, [ 'id'=>'body', 'class' => 'form-control', 'rows' => 3, 'required']) !!}
            </div>
            <div class="form-group">
                <button type="button" onclick="Comentario.formSave()" class="btn btn-primary">
                    Comentar
                </button>
            </div>
        {!! Form::close() !!}
    </div>
</div>

<script type="text/javascript">
	
    $(document).ready(function() {
    
        Comentario = {
            url:"{{ route('Comentar') }}",
            token:"{{csrf_token()}}",
            init:function(){
                $('form').on('submit', function (ev) {
                  ev.preventDefault();
                });

                $('.page-link').on('click',function(event){
                event.preventDefault(); 
                var page = $(this).attr('href').split('page=')[1];
                //alert(page);
                Publicaciones.paginacion(page);

                });
            },
            paginacion:function(page){

                $('#app_container').html('');
                $.ajax({
                url:"{{ route('publicaciones.siguiente') }}",
                method:"POST",
                data:{_token:this.token, page:page},
                success:function(data){
                        //$('#table_data').html(data);
                        $('#app_container').html(data);
                    }
                });
            },
            formSave:function(){

                //alert('aki');
                var formElement = document.getElementById("comentario_form");

                var ajax = App.ajaxData(this.url,formElement);

                if (ajax){
                    $('#panelComentarios').html('');
                    ajax.done(function(data){
                        //alert("Datos Actualizados");
                        $('#body').val('');
                        $('#panelComentarios').html(data);
                        
                    });
                }
            }
        }
        
        Comentario.init();

    });
    
    </script>