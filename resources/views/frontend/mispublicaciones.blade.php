<div class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                <div>
                    <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Mis Publicaciones</div>
                    
                </div>
                <div id="post_update">
                @include('frontend._mipost')
                </div>
                



            </div>

            @include('frontend._fadeRight')
            
        </div>


    </div>
</div>

<script type="text/javascript">
	
    $(document).ready(function() {
    
        Publicaciones = {
            token:"{{csrf_token()}}",
            init:function(){
                $('form').on('submit', function (ev) {
                  ev.preventDefault();
                });

                $('.page-link').on('click',function(event){
                event.preventDefault(); 
                var page = $(this).attr('href').split('page=')[1];
                //alert(page);
                Publicaciones.paginacion(page);

                });
            },
            paginacion:function(page){

                $('#post_update').html('');
                $.ajax({
                url:"{{ route('mispublicaciones.siguiente') }}",
                method:"POST",
                data:{_token:this.token, page:page},
                success:function(data){
                        //$('#table_data').html(data);
                        $('#post_update').html(data);
                        Publicaciones.init();
                    }
                });
            }
        }
        
        Publicaciones.init();

    });
    
    </script>