<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogController@index')->name('inicio');

Route::post('/menu', 'HomeController@menu')->name('menu_url');

Route::post('/sitioweb', 'SitioController@sitioweb')->name('sitioweb');
Route::get('/popular', 'BlogController@popular')->name('popular');
Route::get('/categorias', 'BlogController@categorias')->name('categorias');
Route::get('/masVistos', 'BlogController@masVistos')->name('masVistos');
Route::get('/modificados', 'BlogController@modificados')->name('modificados');
Route::get('/publicaciones', 'BlogController@publicaciones')->name('publicaciones');

Route::get('/populares', 'BlogController@populares_ini')->name('populares.inicio');

Route::post('publicaciones/Siguiente', 'BlogController@siguiente')->name('publicaciones.siguiente');
Route::post('Mispublicaciones/Siguiente', 'Admin\\PostController@siguiente')->name('mispublicaciones.siguiente');
Route::post('ckeditor/image_upload', 'CKEditorController@upload')->name('upload');
Route::post('/Actualizar_sitio', 'SitioController@upload')->name('upload_logo');
Route::post('/Crear_publicacion', 'Admin\\PostController@SavePublicacion')->name('SavePublicacion');
Route::post('/Actualizar_publicacion', 'Admin\\PostController@updatePublicacion')->name('updatePublicacion');



Route::post('/posts/{post}', 'BlogController@post');
Route::post('/posts/{post}/comment', 'BlogController@comment')->middleware('auth');
Route::post('/postscomentar', 'BlogController@comentar')->name('Comentar');

Auth::routes();
Route::get('/profile', 'Auth\\ProfileController@index')->middleware('auth');

Route::get('/sitio', 'SitioController@index')->middleware('auth');

Route::get('/home', 'BlogController@index');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function() {
    Route::resource('/posts', 'PostController');
    Route::put('/posts/{post}/publish', 'PostController@publish')->middleware('admin');
    Route::resource('/categories', 'CategoryController', ['except' => ['show']]);
    Route::resource('/tags', 'TagController', ['except' => ['show']]);
    Route::resource('/comments', 'CommentController', ['only' => ['index', 'destroy']]);
    Route::resource('/users', 'UserController', ['middleware' => 'admin', 'only' => ['index', 'destroy']]);
});
